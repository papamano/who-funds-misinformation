# Acknowledgments

This project received funding from the **EU H2020 Research and Innovation** programme under grant agreements No 830927 (Concordia), No 830929 (CyberSec4Europe), No 871370 (Pimcity) and No 871793 (Accordion), No 101021808 (Spatial), and No 883543 (CC-DRIVER). These results reflect only the authors’ view and the Commission is not responsible for any use that may be made of the information it contains.

## Resources

Project's icon created by [Freepik](https://www.flaticon.com/free-icons/fake-news) - [Flaticon](https://www.flaticon.com/).