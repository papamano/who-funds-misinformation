# Who Funds Misinformation

## About

The data sets provided in this repository were used in the following project. If you use any of these data sets in any way, please cite our work.

Emmanouil Papadogiannakis, Panagiotis Papadopoulos, Evangelos P Markatos, and Nicolas Kourtellis, "[**Who Funds Misinformation? A Systematic Analysis of the Ad-related Profit Routines of Fake News sites**](https://arxiv.org/abs/2202.05079)"

The paper can be found:
* DOI: [10.1145/3543507.3583443](https://doi.org/10.1145/3543507.3583443)
* Open-access version in [arXiv](https://arxiv.org/abs/2202.05079)

## Introduction

Fake news is an age-old phenomenon, widely assumed to be associated with political propaganda published to sway public opinion. Yet, with the growth of social media, it has become a lucrative business for Web publishers. Despite many studies performed and countermeasures proposed, unreliable news sites have increased in the last years their share of engagement among the top performing news sources. Stifling fake news impact depends on our efforts in limiting the (economic) incentives of fake news producers.

In this paper, we aim at enhancing the transparency around these exact incentives, and explore: Who supports the existence of fake news websites via paid ads, either as an advertiser or an ad seller? Who owns these websites and what other Web business are they into? We are the first to systematize the auditing process of fake news revenue flows. We identify the companies that advertise in fake news websites and the intermediary companies responsible for facilitating those ad revenues. We study more than 2,400 popular news websites and show that well-known ad networks, such as Google and IndexExchange, have a direct advertising relation with more than 40% of fake news websites. Using a graph clustering approach on 114.5K sites, we show that entities who own fake news sites, also operate other types of websites pointing to the fact that owning a fake news website is part of a broader business operation.

## Description

This paper contains:

* The list of fake and real news websites used in this work.
* Communities of websites, operated by the same entity, that contain at least on fake news website.
* Annotated screenshots of detected ads in the 50 most popular "News and Media" and the 50 most popular "Sports" websites.

## Notes

* The source code of the crawler used in this work, as well as, the implementation of the ad detection methodology can be found [**here**](https://gitlab.com/papamano/scrape-titan).

* This project is publicly available in order to contribute to the scientific community and to support further research on misinformation.

## Contributors

* [Papadogiannakis Manos](https://gitlab.com/papamano/)

## Support

Please contact one of the project's contributors.

## License

This project is released under under the Mozilla Public License Version 2.0

Make sure you read the provided End-User License Agreement (EULA).
By installing, copying or otherwise using this Software, you agree to be bound
by the terms of the provided End EULA